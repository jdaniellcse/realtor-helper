﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace realtor_helper.Views.ViewModels
{
    public class AssignedFeatureData
    {
        public int FeatureID { get; set; }
        public string Description { get; set; }
        public bool Assigned { get; set; }
    }
}