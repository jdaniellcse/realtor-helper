﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using realtor_helper.Controllers;
using realtor_helper.Models;

namespace realtor_helper.Views.ViewModels
{
    public class AgentIndexData
    {
        public IEnumerable<Agent> Agents { get; set; }
        public IEnumerable<House> House { get; set; }
        public IEnumerable<Feature> Features { get; set; }
    }
}