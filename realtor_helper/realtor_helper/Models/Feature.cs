﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace realtor_helper.Models
{
    public class Feature
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int FeatureID { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual ICollection<House> Houses { get; set; }
    }
}