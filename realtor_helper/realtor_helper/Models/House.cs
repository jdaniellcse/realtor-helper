﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace realtor_helper.Models
{
    public class House
    {
        [HiddenInput(DisplayValue = false)]
        public int HouseID { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        public string Description { get; set; }

        public int AgentID { get; set; }

        public virtual Agent Agent { get; set; }

 
        public virtual ICollection<Feature> Features { get; set; }
    }
}