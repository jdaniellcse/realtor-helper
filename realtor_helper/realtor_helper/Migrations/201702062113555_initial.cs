namespace realtor_helper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agent",
                c => new
                    {
                        AgentID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone = c.Double(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AgentID);
            
            CreateTable(
                "dbo.House",
                c => new
                    {
                        HouseID = c.Int(nullable: false, identity: true),
                        Address = c.String(nullable: false),
                        Price = c.Double(nullable: false),
                        Description = c.String(nullable: false),
                        AgentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HouseID)
                .ForeignKey("dbo.Agent", t => t.AgentID, cascadeDelete: true)
                .Index(t => t.AgentID);
            
            CreateTable(
                "dbo.Feature",
                c => new
                    {
                        FeatureID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.FeatureID);
            
            CreateTable(
                "dbo.FeatureHouse",
                c => new
                    {
                        Feature_FeatureID = c.Int(nullable: false),
                        House_HouseID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Feature_FeatureID, t.House_HouseID })
                .ForeignKey("dbo.Feature", t => t.Feature_FeatureID, cascadeDelete: true)
                .ForeignKey("dbo.House", t => t.House_HouseID, cascadeDelete: true)
                .Index(t => t.Feature_FeatureID)
                .Index(t => t.House_HouseID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FeatureHouse", "House_HouseID", "dbo.House");
            DropForeignKey("dbo.FeatureHouse", "Feature_FeatureID", "dbo.Feature");
            DropForeignKey("dbo.House", "AgentID", "dbo.Agent");
            DropIndex("dbo.FeatureHouse", new[] { "House_HouseID" });
            DropIndex("dbo.FeatureHouse", new[] { "Feature_FeatureID" });
            DropIndex("dbo.House", new[] { "AgentID" });
            DropTable("dbo.FeatureHouse");
            DropTable("dbo.Feature");
            DropTable("dbo.House");
            DropTable("dbo.Agent");
        }
    }
}
