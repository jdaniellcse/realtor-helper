using System.Collections.Generic;
using realtor_helper.Models;

namespace realtor_helper.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<realtor_helper.DAL.ListingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(realtor_helper.DAL.ListingContext context)
        {
            var features = new List<Feature>
            {
                new Feature {Description = "red brick"}
            };

            features.ForEach(s => context.Features.AddOrUpdate(p => p.Description, s));
            features.ForEach(s => context.Features.Add(s));

            var houses = new List<House>
            {
                new House {Address = "123 Test Street", Price = 120000, Description = "House With Features", Features = new List<Feature>()}
            };

            houses.ForEach(s => context.Houses.AddOrUpdate(p => p.Address, s));
            houses.ForEach(s => context.Houses.Add(s));

            var agents = new List<Agent>
            {
                new Agent {FirstName = "John", LastName = "Jones", Phone = 4041231234, Email = "jjones@gmail.com"},

            };

            agents.ForEach(s => context.Agents.AddOrUpdate(p => p.Phone, s));
            agents.ForEach(s => context.Agents.Add(s));



            context.SaveChanges();
        }
    }
}
