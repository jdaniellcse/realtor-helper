﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace realtor_helper.DAL
{
    public class ListingConfiguration : DbConfiguration
    {
        public ListingConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}