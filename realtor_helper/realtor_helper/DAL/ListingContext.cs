﻿using realtor_helper.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace realtor_helper.DAL
{
    public class ListingContext : DbContext
    {
        public DbSet<House> Houses { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Feature> Features { get; set; }


        public ListingContext() : base("realtor_helper2")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}