﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using realtor_helper.DAL;
using realtor_helper.Models;
using realtor_helper.Views.ViewModels;

namespace realtor_helper.Controllers
{
    [Authorize(Roles ="Admin")]
    public class HousesController : Controller
    {
        private ListingContext db = new ListingContext();

        // GET: House
        public ActionResult Index()
        {
            var houses = db.Houses.Include(a => a.Agent).Include(h => h.Features);
            return View(houses);
        }
        [HttpGet]
        // GET: House/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            House house = db.Houses.Find(id);
            
            if (house == null)
            {
                return HttpNotFound();
            }
            
            return View(house);
        }

        // GET: House/Create
        public ActionResult Create()
        {
            var house = new House();
            house.Features = new List<Feature>();

            this.PopulateAssignedFeatures(house);

            PopulateAgentsDropDownList();
            return View();
        }

        // POST: House/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Address,Price,Description,AgentID")] House house, string[] selectedFeatures)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    house.Features = new List<Feature>();
                    PopulateAssignedFeatures(house);

                    if (selectedFeatures != null)
                    {
                        foreach (var feature in selectedFeatures)
                        {
                            var featureAdd = db.Features.Find(int.Parse(feature));
                            house.Features.Add(featureAdd);
                        }
                    }

                    db.Houses.Add(house);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            } catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }

            
            PopulateAgentsDropDownList(house.AgentID);
            return View(house);
        }

        // GET: House/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            House house = db.Houses
                .Include(i => i.Agent)
                .Include(i => i.Features)
                .Single(i => i.HouseID == id);

            PopulateAgentsDropDownList(house);
            PopulateAssignedFeatures(house);
            if (house == null)
            {
                return HttpNotFound();
            }
            return View(house);
        }



        // POST: House/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] selectedFeatures)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var houseToUpdate = db.Houses
               .Include(i => i.Agent)
               .Include(i => i.Features)
               .Single(i => i.HouseID == id);

            if (TryUpdateModel(houseToUpdate, "",
               new string[] { "Address", "Price", "Description", "AgentID" }))
            {
                try
                {
    
                    UpdateHouseFeatures(selectedFeatures, houseToUpdate);

                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            PopulateAssignedFeatures(houseToUpdate);
            return View(houseToUpdate);
        }
        // GET: House/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return HttpNotFound();
            }
            return View(house);
        }

        // POST: House/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            House house = db.Houses.Find(id);
            db.Houses.Remove(house);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       private void PopulateAgentsDropDownList(object selectedAgent = null)
        {
            var agentsQuery = from d in db.Agents
                                   orderby d.LastName
                                   select d;
            ViewBag.AgentID = new SelectList(agentsQuery, "AgentID", "FullName", selectedAgent);
        }

        private void PopulateAssignedFeatures(House house)
        {

            if (house.Features == null)
            {
                house.Features = new List<Feature>();
            }

            var allFeatures = db.Features;
            var houseFeatures = new HashSet<int>(house.Features.Select(c => c.FeatureID));
            var viewModel = new List<AssignedFeatureData>();
            foreach (var feature in allFeatures)
            {
                viewModel.Add(new AssignedFeatureData
                {
                     FeatureID = feature.FeatureID,
                     Description= feature.Description,
                     Assigned = houseFeatures.Contains(feature.FeatureID)
                });
            }
            ViewBag.Features = viewModel;
        }

        private void UpdateHouseFeatures(string[] selectedFeatures, House houseToUpdate)
        {
            if (selectedFeatures == null)
            {
                selectedFeatures = new string[] {};
            }

            var selectedFeaturesTemp = new HashSet<string>(selectedFeatures);
            var houseFeatures = new HashSet<int>(houseToUpdate.Features.Select(c => c.FeatureID));
            foreach (var feature in db.Features)
            {
                if (selectedFeaturesTemp.Contains(feature.FeatureID.ToString()))
                {
                    if (!houseFeatures.Contains(feature.FeatureID))
                    {
                        houseToUpdate.Features.Add(feature);
                    }
                }
                else
                {
                    if (houseFeatures.Contains(feature.FeatureID))
                    {
                        houseToUpdate.Features.Remove(feature);
                    }
                }
            }
        }
    }
}
